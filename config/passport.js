var JwtStrategy = require('passport-jwt').Strategy;

// load up the user model
var Student = require('../models/student');
var Advisor = require('../models/advisor');
var config = require('../config/database'); // get db config file

module.exports = function (passport) {
  var opts = {};
  opts.secretOrKey = config.secret;
  passport.use(new JwtStrategy(opts, function (jwt_payload, done) {
    Student.findOne({ id: jwt_payload.id }, function (err, student) {
          if (err) {
            return done(err, false);
          }

          if (student) {
            done(null, student);
          } else {
            done(null, false);
          }
        });
  }));
};

module.exports = function (passport) {
  var opts = {};
  opts.secretOrKey = config.secret;
  passport.use(new JwtStrategy(opts, function (jwt_payload, done) {
      Advisor.findOne({ id: jwt_payload.id }, function (err, advisor) {
        if (err) {
          return done(err, false);
        }

        if (advisor) {
          done(null, advisor);
        } else {
          done(null, false);
        }
      });
    }));
};
