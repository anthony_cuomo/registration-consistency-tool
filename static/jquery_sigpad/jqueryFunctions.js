$(document).ready(function () {
  //This function allows students to copy courses directly from the adviser schedule
  $('#suggestedScheduleList li').click(function () {
    if ($('#registeredCourseList li').length == $('#suggestedScheduleList li').length) {} else {
      var $clonedAdviserSchedule = $(this).clone();
      $('#registeredCourseList').append($clonedAdviserSchedule);
    }
  });

  //This function allows students to add courses from the course list
  $('#searchTableCourses li').click(function () {
    if ($('#registeredCourseList li').length == $('#suggestedScheduleList li').length) {} else {
      // openModal();
      var $clonedCourseSchedule = $(this).clone();
      $('#registeredCourseList').append($clonedCourseSchedule);
    }
  });

  //This function allows students to delete courses from their schedule
  $('#registeredCourseList').on('click', 'li', function () {
    $(this).remove();
    if ($('#registeredCourseList li').length == 0) {
      $('#suggestedScheduleList li').each(function () {
        $('#suggestedScheduleList li').css('background-color', '');
      });
    }
  });

  //These functions check the student schedule against the adviser schedule and color classes accordingly
  function compareSchedulesStudent() {
    for (var i = 0; i < $('#registeredCourseList li').length; i++) {
      $('#suggestedScheduleList li').each(function () {
        if ($('#registeredCourseList li:nth-child(' + (i + 1) + ')').text() == $(this).text()) {
          $('#registeredCourseList li:nth-child(' + (i + 1) + ')').css('background-color', 'lightgreen');
          return false;
        } else {
          if ($('#registeredCourseList li:nth-child(' + (i + 1) + ')').css('background-color') == 'lightgreen') {} else {
            $('#registeredCourseList li:nth-child(' + (i + 1) + ')').css('background-color', 'red');
            return false;
          }
        }
      });
    }
  };

  function compareSchedulesAdviser() {
    for (var i = 0; i < $('#suggestedScheduleList li').length; i++) {
      $('#registeredCourseList li').each(function () {
        if ($('#suggestedScheduleList li:nth-child(' + (i + 1) + ')').text() == $(this).text()) {
          $('#suggestedScheduleList li:nth-child(' + (i + 1) + ')').css('background-color', 'lightgreen');
          return false;
        } else {
          if ($('#suggestedScheduleList li:nth-child(' + (i + 1) + ')').css('background-color') == 'lightgreen') {} else {
            $('#suggestedScheduleList li:nth-child(' + (i + 1) + ')').css('background-color', 'red');
          }
        }
      });
    }
  }

  setInterval(compareSchedulesStudent, 10);
  setInterval(compareSchedulesAdviser, 10);



  //These function deal with the modals being created
  // CURRENTLY NOT BEING USED DUE TO INABILITY TO PROPERLY CONNECT WITH MONGODB
  // function openModal() {
  //   $('[data-popup-open]').on('click', function(e) {
  //     var targeted_popup_class = jQuery(this).attr('data-popup-open');
  //     $('[data-popup="' + targeted_popup_class + '"]').fadeIn(350);
  //     e.preventDefault();
  //   });
  //   $('[data-popup-close]').on('click', function(e) {
  // 	  var targeted_popup_class = jQuery(this).attr('data-popup-close');
  // 	  $('[data-popup="' + targeted_popup_class + '"]').fadeOut(350);
  // 	  e.preventDefault();
  //   });
  // }

});
