$(document).ready(function () {
  //Add courses to the recommended schedule from the course list
  $('#recommended-courseList li').click(function () {
    var $clonedAdviserCourse = $(this).clone();
    $('#recommendedSchedule').append($clonedAdviserCourse);
  });

  $('#registered-courseList li').click(function () {
    var $clonedStudentCourse = $(this).clone();
    $('registeredSchedule').append($clonedStudentCourse);
  });

  $('#recommendedSchedule').on('click', 'li', function () {
    $(this).remove();
  });

  $('#registeredSchedule').on('click', 'li', function () {
    $(this).remove();
  });
});
