/* app module */

var rctApp = angular.module('rctApp', ['ngRoute', 'rctControllers', 'rctServices']);

rctApp.config(['$routeProvider', '$locationProvider', function ($routeProvider,
$locationProvider) {
  $routeProvider
  .when('/', {
    templateUrl: 'partials/login.html',
    controller: 'loginCtrl',
  })
  .when('/register', {
    templateUrl: 'partials/register.html',
    controller: 'registerCtrl',
  })
  .when('/student', {
    templateUrl: 'partials/main_student.html',
    controller: 'studentCtrl',
  })
  .when('/advisor', {
    templateUrl: 'partials/main_advisor.html',
    controller: 'advisorCtrl',
  })
  .when('/advisor-signature', {
          templateUrl: 'partials/advisor_signature.html',
          controller: 'advisorCtrl', })
  .when('advisor/student/:student_id', {
    templateUrl: 'partials/test.html',
    controller: 'advisorCtrl',
  })
  .when('/student/advisor/signature', {
    templateUrl: 'partials/advisorSig.html',
    controller: 'advisorCtrl',
  })
  .when('/course', {
    templateUrl: 'partials/course_add.html',
    controller: 'courseCtrl',
  });
  $locationProvider.html5Mode(false).hashPrefix('!');

},
]);
