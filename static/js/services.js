var rctServices = angular.module('rctServices', ['ngResource']);

rctServices.factory('AuthService', ['$q', '$http',
 function ($q, $http) {
  var LOCAL_TOKEN_KEY = 'danthemachinedresselhouse';
  var isAuthenticated = false;
  var authToken;

  function loadUserCredentials() {
    var token = window.localStorage.getItem(LOCAL_TOKEN_KEY);
    if (token) {
      useCredentials(token);
    }
  }

  function storeUserCredentials(token) {
    window.localStorage.setItem(LOCAL_TOKEN_KEY, token);
    useCredentials(token);
  }

  function useCredentials(token) {
    isAuthenticated = true;
    authToken = token;

    // Set the token as header for your requests!
    $http.defaults.headers.common.Authorization = authToken;
  }

  function destroyUserCredentials() {
    authToken = undefined;
    isAuthenticated = false;
    $http.defaults.headers.common.Authorization = undefined;
    window.localStorage.removeItem(LOCAL_TOKEN_KEY);
  }

  // Registration

  var studentRegister = function (student) {
    return $q(function (resolve, reject) {
      $http.post('/student/signup', student).then(function (result) {
        if (result.data.success) {
          resolve(result.data.msg);
        } else {
          reject(result.data.msg);
        }
      });
    });
  };

  var advisorRegister = function (advisor) {
    return $q(function (resolve, reject) {
      $http.post('/advisor/signup', advisor).then(function (result) {
        if (result.data.success) {
          resolve(result.data.msg);
        } else {
          reject(result.data.msg);
        }
      });
    });
  };

  // Login and authenitcation

  var studentLogin = function (student) {
    return $q(function (resolve, reject) {
      $http.post('/student/authenticate', student).then(function (result) {
        if (result.data.success) {
          storeUserCredentials(result.data.token);
          resolve(result.data.msg);
        } else {
          reject(result.data.msg);
        }
      });
    });
  };

  var advisorLogin = function (advisor) {
    return $q(function (resolve, reject) {
      $http.post('/advisor/authenticate', advisor).then(function (result) {
        if (result.data.success) {
          storeUserCredentials(result.data.token);
          resolve(result.data.msg);
        } else {
          reject(result.data.msg);
        }
      });
    });
  };

  var addCourse = function (course) {
    return $q(function (resolve, reject) {
      $http.post('/course/addCourse', course).then(function (result) {
        if (result.data.success) {
          resolve(result.data.msg);
        } else {
          reject(result.data.msg);
        }
      });
    });
  };

  var logout = function () {
    destroyUserCredentials();
  };

  function getStudentInfo() {
    return studentInfo;
  }

  loadUserCredentials();

  return {
    studentRegister: studentRegister,
    advisorRegister: advisorRegister,
    getStudentInfo: getStudentInfo,
    studentLogin: studentLogin,
    advisorLogin: advisorLogin,
    addCourse: addCourse,
    logout: logout,
    isAuthenticated: function () {return isAuthenticated;},
  };
},
]);

rctServices.factory('AuthInterceptor', ['$rootScope', '$q', function ($rootScope, $q) {
  return {
      responseError: function (response) {
        $rootScope.$broadcast({
          401: 'auth-not-authenticated',
        }[response.status], response);
        return $q.reject(response);
      },
    };
},

]);

rctServices.config(function ($httpProvider) {
  $httpProvider.interceptors.push('AuthInterceptor');
});

// rest service for getting students from the database

rctServices.factory('StudentData', ['$http', '$routeParams', function ($http, $routeParams) {
  var urlBase = '/advisor/student/';
  var studentData = {};

  studentData.getStudents = function () {
    return $http.get(urlBase);
  };

  return studentData;

},

]);

rctServices.factory('Student', ['$resource', function ($resource) {
  return $resource('/advisor/student/:studentId', null, {
    get: { method: 'GET' },

  });
},

 ]);

rctServices.factory('CourseData', ['$http', '$routeParams', function ($http, $routeParams) {
  var urlBase = '/student/course/';
  var courseData = {};

  courseData.getCourses = function () {
    return $http.get(urlBase);
  };

  return courseData;

},

]);
