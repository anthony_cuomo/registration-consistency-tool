app.constant(AUTH_EVENTS, {
  notAuthenticated: 'auth-not-authenticated',
})
.constant(API_ENDPOINT, {
  studentUrl: 'http://127.0.0.1:3000/student',
  advisorUrl: 'http://127.0.0.1:3000/advisor',
});
