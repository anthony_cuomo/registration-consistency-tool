/* Controllers */

// Main controller module
var rctControllers = angular.module('rctControllers', []);

// login controller
rctControllers.controller('loginCtrl', ['$scope', '$location', 'AuthService', '$window',
 function loginCtrl($scope, $location, AuthService, $window) {

  //student login

  $scope.student = {
    student_email: '',
    student_password: '',
  };

  $scope.studentSignIn = function () {
    AuthService.studentLogin($scope.student).then(function (msg) {
      $location.path('/student');
    },

    function (errMsg) {
      var alertPop = $window.alert('Login Failed Try Again!!');
    });
  };

  // advisor Login

  $scope.advisor = {
    prof_email: '',
    prof_password: '',
  };

  $scope.advisorSignIn = function () {
    AuthService.advisorLogin($scope.advisor).then(function (msg) {
      $location.path('/advisor');
    },

    function (errMsg) {
      var alertPop = $window.alert('Login Failed Try Again!!');
    });
  };

},
]);

// registration controller
rctControllers.controller('registerCtrl', ['$scope', '$location', 'AuthService', '$window',
 function registerCtrl($scope, $location, AuthService, $window) {

  //advisor signup

  $scope.advisor = {
    first_name: '',
    last_name: '',
    prof_email: '',
    prof_password: '',
    advisorConfirmPassword: '',
  };
  $scope.advisorSignup = function () {
    AuthService.advisorRegister($scope.advisor).then(function (msg) {
      $location.path('/');
      var alertPop = $window.alert('Advisor Registration success!');

    }, function (err) {

        var alertPop = $window.alert('Advisor Registration failed!');
      });
  };

  // student studentSignup

  $scope.student = {
    first_name: '',
    last_name: '',
    student_email: '',
    student_password: '',
    confirmPassword: '',

  };
  $scope.studentSignup = function () {
    AuthService.studentRegister($scope.student).then(function (msg) {
      $location.path('/');
      var alertPop = $window.alert('Student Registration Success!');

    }, function (err) {

        var alertPop = $window.alert('Student Registration Failed!');
      });
  };
},
]);

// student controller
rctControllers.controller('studentCtrl', ['$scope', '$http', 'AuthService', '$window', '$route', '$location',
'CourseData', '$routeParams', function studentCtrl($scope, $http, AuthService, $window, $route, $location,
CourseData, $routeParams) {

  getCourses();

  //student logout
  $scope.destroyStudentSession = function () {
    AuthService.logout();
  };

  $scope.getInfo = function () {
    $http.get('/student/studenthome').then(function (result) {
      $scope.memberinfo = result.data.msg;
    });
  };

  $scope.addSig = function () {
      $window.alert('Advisor Approval Added!');
      $scope.approveForm.$setPristine();

    };

  // $scope.getCourse = function () {
  //
  //   $http.get('student/course/' + $routeParams.courseId).then(function (result) {
  //     $scope.courseInfo = result.data;
  //     console.log(result);
  //   });
  // };
  $scope.edit = false;

  $scope.freezeSchedule = function () {

  };

  $scope.toggleEditMode = function () {
        $scope.edit = !$scope.edit;
      };

  $scope.suggestedSchedule = [];
  $scope.registeredSchedule = [];
  var flag = false;
  $scope.addToSchedule = function (course) {
    if (flag == false) {
      $scope.suggestedSchedule.push(course);
      console.log($scope.suggestedSchedule);
      if ($scope.suggestedSchedule.length > 4) {
        flag = true;
      }
    } else if (flag == true) {
      if ($scope.suggestedSchedule.length > 4) {
        $scope.registeredSchedule.push(course);
        console.log($scope.registeredSchedule);
        if ($scope.registeredSchedule.length > 4) {
          if ($scope.suggestedSchedule.length <= 4) {
            flag = false;
          } else {
            flag = null;
          }
        }
      } else {
        flag = false;
      }
    } else {
      console.log('how Are YA!!!');
    }
  };

  $scope.saveSchedule = function () {
    $window.alert('Student Recommended Schedule Saved Successfully!');
  };

  $scope.removeFromSchedule = function (course) {
    var index = $scope.suggestedSchedule.indexOf(course);
    $scope.suggestedSchedule.splice(index, 1);
  };

  $scope.removeFromRegSchedule = function (course) {
    var index = $scope.registeredSchedule.indexOf(course);
    $scope.registeredSchedule.splice(index, 1);
  };

  $scope.getCourse = function (course) {
    var courseOne = $scope.suggestedSchedule.indexOf(course);
    var value = $scope.suggestedSchedule[courseOne];
    console.log(value);
  };

  // have this method take in both schedules and image to be saved to database
  // once backend is written

  $scope.signature = [];
  $scope.saveSignedSchedule = function (img) {
    var signature = $scope.signature.push(img);
    $window.alert('Student Schedules Saved Successfully!');
    $route.reload();
    console.log(signature);
  };

  // get all courses from database
  function getCourses() {
    CourseData.getCourses().then(function (res) {
      $scope.courses = res.data;
    },

    function (err) {

      $scope.status = 'Unable to load courses: ' + err.message;
    });
  }
},
]);

rctControllers.controller('advisorCtrl', ['$scope', 'StudentData', 'Student', '$location',
'$routeParams', '$http', '$window', function advisorCtrl($scope, StudentData,
Student, $location, $routeParams, $http, $window) {

  //getStudents();

  //advisor logout
  $scope.destroyAdvisorSession = function () {
    AuthService.logout();
  };

  $scope.getInfo = function () {
    $http.get('/advisor/advisorhome').then(function (result) {
      $scope.memberinfo = result.data.msg;
    });
  };

  // // get all students from database
  // function getStudents() {
  //   StudentData.getStudents().then(function (res) {
  //     $scope.students = res.data;
  //   },
  //
  //   function (err) {
  //
  //     $scope.status = 'Unable to load students: ' + err.message;
  //   });
  // }

  $scope.signature = [];
  $scope.addImg = function (img) {
    var signature = $scope.signature.push(img);
    $window.alert('Signature Image Saved Successfully!');
    $location.path('/advisor');
    console.log(signature);
  };

},
]);

rctControllers.controller('courseCtrl', ['$scope', 'AuthService', '$location', '$window',
function courseCtrl($scope, AuthService, $location, $window) {

  //add course

  $scope.course = {

    course_name: '',
    course_subject: '',
    course_num: '',
    course_title: '',
    course_desc: '',
    coures_identifier: '',

  };

  $scope.addCourse = function () {
    AuthService.addCourse($scope.course).then(function (msg) {
      $location.path('/course');
      var alertPop = $window.alert('Course add success!');

    }, function (err) {

        var alertPop = $window.alert('Course add failed!');
      });
  };

},

]);
