// Course Schema

/******
course indentifier Numbers and meanings:
  * 0 == MER -> non Elective
  * 1 == MER -> elective
  * 2 == ENR
  * 3 == Foundational Courses
  * 4 == UC courses
******/

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var CourseSchema = Schema({
  course_name: { type: String, max: 50 },
  course_subject: { type: String, max: 25 },
  course_num: { type: String },
  course_title: { type: String, max: 25 },
  course_desc: { type: String, max: 1000 },
  course_identifier: { type: Number },
});

module.exports = mongoose.model('Course', CourseSchema);
