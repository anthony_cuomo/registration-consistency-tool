// Student Schema
var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var bcrypt = require('bcrypt');
var schedule = require('./schedule.js');
var ScheduleSchema = mongoose.model('Schedule').schema;

var StudentSchema = Schema({
    first_name: { type: String, unique: true, required: true, max: 50 },
    last_name: { type: String, unique: true, required: true, max: 50 },
    student_email: { type: String, required: true, max: 30 },
    student_password: { type: String, required: true, max: 20 },
    schedule: [ScheduleSchema],
  });

StudentSchema.pre('save', function (next) {
  var student = this;
  if (this.isModified('password') || this.isNew) {
    bcrypt.genSalt(10, function (err, salt) {
        if (err) {
          return next(err);
        }

        bcrypt.hash(student.student_password, salt, function (err, hash) {
            if (err) {
              return next(err);
            }

            student.student_password = hash;
            next();
          });
      });
  } else {
    return next();
  }
});

StudentSchema.methods.comparePassword = function (passw, cb) {
    bcrypt.compare(passw, this.student_password, function (err, isMatch) {
        if (err) {
          return cb(err);
        }

        cb(null, isMatch);
      });
  };

module.exports = mongoose.model('Student', StudentSchema);
