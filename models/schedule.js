// Schedule Schema

/*****
    * Suggested Schedule == 0
    * Registered Schedule == 1
******/

var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var course = require('./course.js');
var CourseSchema = mongoose.model('Course').schema;

var ScheduleSchema = Schema({
  schedule_name: { type: String, max: 20 },
  schedule_identifier: { type: Number },
  course: [CourseSchema],
});

module.exports = mongoose.model('Schedule', ScheduleSchema);
