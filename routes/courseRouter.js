var express = require('express');
var courseRouter = express.Router();
var bodyParser  = require('body-parser');
var mongoose = require('mongoose');
var Course = require('../models/course');
var config = require('../config/database');

/* Retrieve All Courses*/
courseRouter.get('/', function (req, res) {
  Course.find({}, function (err, courses) {
    if (err) throw err;
    res.json(courses);

    res.end('retrieve all courses');
  });

});

/* add course to database to be used for scheduling */
courseRouter.post('/addCourse', function (req, res) {
  if (!req.body.course_name || !req.body.course_num) {
    res.json({ success: false, msg: 'Please pass course name and course number' });
  } else {
    var newCourse = new Course({
      course_name: req.body.course_name,
      course_subject: req.body.course_subject,
      course_num: req.body.course_num,
      course_title: req.body.course_title,
      course_desc: req.body.course_desc,
      course_identifier: req.body.course_identifier,
    });

    // save the course
    newCourse.save(function (err) {
      if (err) {
        console.log(err);
        return res.json({ success: false, msg: 'Course already exists.' });
      }

      res.json({ success: true, msg: 'Successful created new course.' });
    });
  }
});

module.exports = courseRouter;
