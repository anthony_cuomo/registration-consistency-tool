var express = require('express');
var advisorRouter = express.Router();
var bodyParser  = require('body-parser');
var mongoose = require('mongoose');
var Advisor = require('../models/advisor');
var Student = require('../models/student');
var passport	= require('passport');
var config = require('../config/database'); // get db config file
var jwt = require('jwt-simple');
var Passport = require('../config/passport');
var Verify = require('./verify');
var ObjectID = require('mongodb').ObjectID;

// all routes start with /advisor

/* get all advisors for testing */
advisorRouter.get('/', function (req, res) {
  Advisor.find({}, function (err, advisors) {
    if (err) throw err;
    res.json(advisors);

    res.end('retrieve all Advisors');
  });

});

/* Register Advisor create a new user account /advisor/signup */
advisorRouter.post('/signup', function (req, res) {
  if (!req.body.first_name || !req.body.prof_password) {
    res.json({ success: false, msg: 'Please pass name and password.' });
  } else {
    var newAdvisor = new Advisor({
      first_name: req.body.first_name,
      last_name: req.body.last_name,
      prof_email: req.body.prof_email,
      prof_password: req.body.prof_password,
    });

    // save the advisor
    newAdvisor.save(function (err) {
      if (err) {
        console.log(err);
        return res.json({ success: false, msg: 'Username already exists.' });
      }

      res.json({ success: true, msg: 'Successful created new advisor.' });
    });
  }
});

// route to authenticate a advisor post advisor/authenticate
advisorRouter.post('/authenticate', function (req, res) {
  Advisor.findOne({
    prof_email: req.body.prof_email,
  }, function (err, advisor) {
      console.log(advisor);
      if (err) throw err;

      if (!advisor) {
        res.send({ success: false, msg: 'Authentication failed. User not found' });

      } else {
        // check if password matches
        advisor.comparePassword(req.body.prof_password, function (err, isMatch) {
          if (isMatch && !err) {

            // if user is found and password is right create token
            var token = jwt.encode(advisor, config.secret);

            // return the info including token as json
            res.json({ success: true, token: 'JWT ' + token });
          }else {
            res.send({ success: false, msg: 'Authentication failed. Wrong password' });
          }
        });
      }
    });
});

/* login Advisor
 route to a advisor home /advisor/advisorhome */

advisorRouter.get('/advisorhome', passport.authenticate('jwt', { session: false }),
function (req, res) {
  var token = Verify.getToken(req.headers);
  if (token) {
    var decoded = jwt.decode(token, config.secret);
    Advisor.findOne({
      first_name: decoded.first_name,
    }, function (err, advisor) {
      if (err) throw err;
      if (!advisor) {
        return res.status(403).send({ success: false,
          msg: 'Authentication failed. User not found', });
      }else {
        res.json({ success: true, msg: 'Welcome in the member area ' + advisor.first_name + '!' });
      }
    });

  }else {
    return res.status(403).send({ success: false, msg: 'No Token provided' });
  }
});

advisorRouter.get('/student', function (req, res) {
  Student.find({}, function (err, students) {
    if (err) throw err;
    res.json(students);

    res.end('retrieve all students');
  });

});

advisorRouter.get('/student/:studentId', function (req, res, next) {

              var paramsID = req.params.studentId;

              Student.findOne({
                username: paramsID,
              }, function (err, student) {
                if (student == null) {
                  res.status(401).send('Student info is null');
                }else {
                  res.status(200).send({
                    _id: student.studentId,
                    first_name: student.first_name,
                    last_name: student.last_name,
                    student_email: student.student_email,
                  });
                }
              });

              // Student.findById(req.params.studentId, function (err, student) {
              //   if (err) throw err;
              //   console.log(student);
              //   res.json(student);
              // });
            });

module.exports = advisorRouter;
