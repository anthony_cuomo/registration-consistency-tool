var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var mongoose = require('mongoose');
var passport = require('passport');
var config = require('./config/database'); // get db config file
var advisor = require('./models/advisor');
var student = require('./models/student');
var schedule = require('./models/schedule');
var course = require('./models/course');
var advisorRouter = require('./routes/advisorRouter');
var studentRouter = require('./routes/studentRouter');
var courseRouter = require('./routes/courseRouter');

//Connect to the Mongo DB
mongoose.connect(config.database);
var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function () {
  console.log('Connected correctly to server');
});

// passport
require('./config/passport')(passport);

var app = express();

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use('/', express.static(path.join(__dirname, './static')));
app.use('/advisor', advisorRouter);
app.use('/student', studentRouter);
app.use('/course', courseRouter);

module.exports = app;
